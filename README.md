# Bring

Simple static site generator made for my use.

## Installing

```console
$ git clone https://gitlab.com/Marcu5H/bring.git
$ cd bring
$ cargo install --path .
```

## Usage

1. Create a new bring site: `bring new <site name> && cd <site name>`.
1. Edit `bring_config.toml` and edit the domain field to your domain or IP address.
1. Write the pages you want in markdown and place them in `pages/`.
1. Edit the stylesheet located at `static/stylesheet.css`.
1. Build the site with `bring build` and the site will be placed in `pub/`.
