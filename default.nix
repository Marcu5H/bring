{ lib
, fetchFromGitLab
, rustPlatform
}:
rustPlatform.buildRustPackage rec {
  pname = "bring";
  version = "0.1.0";

  src = fetchFromGitLab {
    owner = "Marcu5H";
    repo = pname;
    rev = "v0.1.0";
    hash = "sha256-++OtHjtXsiN2P+bHugfqBvtmWTvVBgbijjGOei7XTnM=";
  };

  cargoHash = "sha256-6X+BXPQXcAbKYzbs/NoDMV8vmFlpaVY0Yc1ffW7SWl8=";

  meta = with lib; {
    description = "Static Site Generator";
    homepage = "https://gitlab.com/Marcu5H/bring";
    license = licenses.gpl3;
    maintainers = [];
  };
}
