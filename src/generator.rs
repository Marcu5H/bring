use std::{
    io::{self, Read, Write},
    path::{PathBuf, Path},
    fs::{self, File, OpenOptions},
};
use pulldown_cmark::{Parser, html};

use crate::site_config::get_config;

fn get_pages(path: &PathBuf) -> io::Result<Vec<PathBuf>> {
    let mut pages: Vec<PathBuf> = Vec::new();

    for page in fs::read_dir(path)? {
        let page = page?;
        let page_path = page.path();
        if page_path.is_dir() {
            if let Ok(more_pages) = get_pages(&page_path) {
                for p in more_pages {
                    pages.push(p);
                }
            }
        } else {
            pages.push(page_path);
        }
    }

    return Ok(pages);
}

fn page_to_html(page_path: &PathBuf) -> io::Result<String> {
    let mut page_file = File::open(&page_path)?;

    let mut page_md = String::new();
    page_file.read_to_string(&mut page_md)?;

    let parser = Parser::new(&page_md);

    let mut page_html = String::new();
    html::push_html(&mut page_html, parser);

    return Ok(page_html);
}

fn change_path_prefix(p: &Path, old: &Path, new: &Path) -> Option<PathBuf> {
    if p.starts_with(old) {
        let changed = new.to_path_buf();
        return Some(changed.join(p.strip_prefix(old).unwrap().to_path_buf()));
    }

    return None;
}

fn copy_dir(from: &Path, to: &Path) -> io::Result<()> {
    for entry in fs::read_dir(from)? {
        let entry = entry?;
        let path = entry.path();
        if path.is_dir() {
            copy_dir(&path, &to)?;
        } else if let Some(static_location) =
            change_path_prefix(&path, from, to) {
            // TODO: remove unwrap call
            if !static_location.parent().unwrap().exists() {
                fs::create_dir(static_location.parent().unwrap())?;
            }
            fs::copy(path, static_location)?;
        }
    }

    return Ok(());
}

pub fn build_site(release: bool) -> io::Result<()> {
    let config_file_path = PathBuf::from("bring_config.toml");
    if !config_file_path.exists() {
        return Err(io::Error::new(io::ErrorKind::Other,
                                  "no bring site in current directory"));
    }

    let config = match get_config(config_file_path, release) {
        Ok(c) => c,
        Err(_) =>  {
            return Err(io::Error::new(io::ErrorKind::Other,
                                    "failed to get config file"));
        }
    };

    if !config.site.pages_path.exists() {
        return Err(io::Error::new(io::ErrorKind::Other,
                                "defined pages path does not exist"));
    }
    if !config.site.static_path.exists() {
        return Err(io::Error::new(io::ErrorKind::Other,
                                "defined static path does not exist"));
    }

    let pub_site_path = PathBuf::from("pub");
    if !pub_site_path.exists() {
        fs::create_dir("pub")?;
    }
    if pub_site_path.is_file() {
        return Err(io::Error::new(io::ErrorKind::Other,
                                "`pub` exists and is a file"));
    }

    let pages = get_pages(&config.site.pages_path)?;

    println!("Building {} page(s)...", pages.len());

    for page in pages {
        print!("Building `{}`...", page.to_string_lossy());
        if let Ok(page_html) = page_to_html(&page) {
            let full_page_html = format!( // TODO: add custom title
                "<!DOCTYPE html><html><head><title>{}</title>\
                 <meta charset=\"utf-8\"/>\
                 <meta name=generator content=\"Bring v{}\">\
                 <link rel=\"stylesheet\" type=\"text/css\" \
                 href=\"//{}/static/stylesheet.css\">\
                 </head><body><div class=\"main-container\">\
                 {}</div></body></html>", config.site.domain,
                env!("CARGO_PKG_VERSION"), config.site.domain, page_html);

            let mut out_file_path = match change_path_prefix(
                &page, &config.site.pages_path, &pub_site_path) {
                Some(p) => p,
                None => {
                    println!(" Failed!");
                    continue;
                }
            };

            out_file_path.set_extension("html");

            if !out_file_path.parent().unwrap().exists() { // TODO: remove unwrap
                fs::create_dir_all(out_file_path.parent().unwrap())?;
            }

            let mut page_file = match OpenOptions::new()
                .write(true)
                .create(true)
                .truncate(true)
                .open(&out_file_path) {
                    Ok(f) => f,
                    Err(e) => {
                        println!(" Failed!: {}", e);
                        continue;
                    }
            };

            match page_file.write_all(full_page_html.as_bytes()) {
                Ok(_) => { },
                Err(e) => {
                    println!(" Failed: {}", e);
                    continue;
                }
            }

            println!(" Finished: `{}`", out_file_path.to_string_lossy());
        } else {
            println!(" Failed!");
        }
    }

    println!("Copying `{}` to `pub/static`",
             config.site.static_path.to_string_lossy());

    copy_dir(&config.site.static_path, &PathBuf::from("pub/static"))?;

    return Ok(());
}
