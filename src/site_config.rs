use std::{
    path::PathBuf, io::{self, Read}, fs::File,
};
use toml;
use serde::Deserialize;

#[derive(Deserialize)]
pub struct Config {
    pub site: PSite,
}

#[derive(Deserialize)]
pub struct PSite {
    pub static_path: PathBuf,
    pub pages_path: PathBuf,
    pub domain: String,
}

#[derive(Deserialize)]
struct Site {
    pub dev: PSite,
    pub release: PSite,
}

pub fn get_config(config_file_path: PathBuf, release: bool) -> io::Result<Config> {
    let mut cf = File::open(config_file_path)?;

    let mut config_string = String::new();
    cf.read_to_string(&mut config_string)?;

    let config: Site = match toml::from_str(&config_string) {
        Ok(c) => c,
        Err(e) => {
            print!("{}", e);
            return Err(io::Error::new(io::ErrorKind::Other,
                                    "failed to parse config file"));
        }
    };

    if release {
        return Ok(Config { site: config.release });
    }

    return Ok(Config { site: config.dev });
}
