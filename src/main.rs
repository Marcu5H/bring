use std::{
    process::exit,
    path::PathBuf,
    fs::{self, File},
    io::{self, Write},
};
use clap::{
    Arg, Command, ArgAction,
};

use bring::generator;

fn init_site(name: &mut PathBuf) -> std::io::Result<()> {
    name.push("bring_config.toml");
    if name.exists() {
        return Err(io::Error::new(io::ErrorKind::Other,
                                  "bring site already exists"));
    }
    name.pop();

    fs::create_dir_all(&name)?;

    name.push("bring_config.toml");
    let mut config_file = File::create(&name)?;
    name.pop();

    config_file
        .write_all(b"[dev]\nstatic_path = 'static/'\npages_path = 'pages/'\
                     \ndomain = '127.0.0.1:8000'\n\n[release]\nstatic_path = \
                     'static/'\npages_path = 'pages/'\ndomain = \
                     'your domain here'")?;

    name.push("static");
    fs::create_dir(&name)?;
    name.push("stylesheet.css");
    let mut stylesheet_file = File::create(&name)?;
    name.pop();
    name.pop();

    stylesheet_file.write_all(
b":root {
    --bg-col: #060819;
    --header-col: #ede978;
    --sub-col: #78aded;
    --font: monospace;
    --list-entry-col: #d6d3c0;
    --par-col: var(--list-entry-col);
}
body {
    background-color: var(--bg-col);
    font-family: var(--font);
}
.main-container {
    margin: 0 auto;
    max-width: 900px;
}
.main-container h1 {
    text-align: center;
    color: var(--header-col);
    font-size: 28pt;
}
.main-container h3 {
    text-align: justify;
    color: var(--sub-col);
    font-size: 20pt;
}
.main-container h5 {
    text-align: center;
    color: var(--list-entry-col);
    font-size: 8pt;
}
.main-container ul {
    color: var(--list-entry-col);
    font-size: 12pt;
}
.main-container p {
    color: var(--par-col);
    font-size: 12pt;
}
.main-container a:hover {
    color: var(--list-entry-col)
}"
    )?;

    name.push("pages");
    fs::create_dir(name)?;

    return Ok(());
}

fn main() {
    let arg_matches = Command::new("bring")
        .about("simple static site generator")
        .version(env!("CARGO_PKG_VERSION"))
        .subcommand_required(true)
        .author("Marcu5h (https://bringeber.dev)")
        .subcommand(
            Command::new("new")
                .arg(
                    Arg::new("name")
                        .action(ArgAction::Set),
                )
        )
        .subcommand(
            Command::new("build")
                .arg(
                    Arg::new("release")
                        .short('r')
                        .long("release")
                        .required(false)
                        .action(ArgAction::SetTrue),
                )
        )
        .get_matches();

    match arg_matches.subcommand() {
        Some(("new", new_matches)) => {
            let site_name: &String =
                match new_matches.get_one::<String>("name") {
                    Some(n) => n,
                    None => {
                        eprintln!("Missing site name!");
                        exit(1);
                    },
            };

            match init_site(&mut PathBuf::from(site_name)) {
                Ok(_) => println!("Successfully created site `{}`", site_name),
                Err(e) => eprintln!("Failed to create site `{}`, reason: `{}`",
                                    site_name, e),
            }
        }
        Some(("build", build_matches)) => {
            match generator::build_site(build_matches.get_flag("release"))
            {
                Ok(_) => println!("Finished build"),
                Err(e) => eprintln!("Failed to build site: `{}`", e),
            }
        }
        _ => unreachable!(),
    }
}
